# keepalive.sh

keepalive.sh is yet another script that unconditionally attempts to keep a
process alive.


## Quick start

```sh
% sh keepalive.sh command
```

This will launch `command` and restart it should it exit.


## Getting started

### Source

keepalive.sh is hosted at GitLab, https://gitlab.com/tsaviran/keepalive .

### Dependencies

 * Any Bourne shell
 * Standard Unix tools: tr, head, sed, expr, sleep


## Usage

```
Usage: keepalive.sh ARG ...
  env can be
    FORK=y		fork process
    BACKOFF_INIT=2	initial backoff time in seconds
    BACKOFF_MULTI=2.0	multiply backoff with f on failure
    BACKOFF_RAND=1.0	multiplier for backoff randomness
    BACKOFF_LIMIT=300	backoff time upper limit in seconds
    BACKOFF_MIN=2	minimum backoff time in seconds
    LIFE_EXPECTANCY=15	expected life time of a process in seconds
    DEBUG=		enable debugging
```


### Examples

```sh
% FORK=y sh keepalive.sh autossh tunnel -N
```

While autossh is an excellent tool, it exits on 0 return code. keepalive.sh can
be used to address this feature.
